describe('[VALIDATE]', () => {
  // testing reference taken from
  // https://medium.com/trabe/mocking-different-values-for-the-same-module-using-jest-a7b8d358d78b

  beforeEach(() => jest.resetModules())

  it('Should return 1 (fail exit) due to no stage provided', async done => {
    const validate = require('../src/core/validate')
    delete process.env.API_STAGE
    jest.mock('../src/util/arguments', () => {
      return { strict: false }
    })
    jest.mock('../src/util/slsprinter', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = async () =>
        Promise.resolve(
          fs.readFileSync(
            require('path').resolve('__tests__', './print-warning.txt'),
            'utf8'
          )
        )
      return slsPrint
    })

    jest.mock('../src/util/parser', () => {
      const fs = jest.requireActual('fs')

      const slsPrintWarning = fs.readFileSync(
        require('path').resolve('__tests__', './print-warning.txt'),
        'utf8'
      )

      const parse = jest.requireActual('../src/util/parser')
      const parsed = () =>
        parse(slsPrintWarning, jest.requireActual('../src/util/config')())
      return parsed
    })
    jest.mock('fs', () => {
      return {
        existsSync: () => true,
        readFileSync: () => false
      }
    })
    const result = await validate()
    expect(result).toBe(1)
    done()
  })

  it('Should return 0 (normal exit) without strict mode', async done => {
    const validate = require('../src/core/validate')

    jest.mock('../src/util/arguments', () => {
      return { strict: false, stage: 'dev' }
    })

    jest.mock('../src/util/slsprinter', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = async () =>
        Promise.resolve(
          fs.readFileSync(
            require('path').resolve('__tests__', './print-warning.txt'),
            'utf8'
          )
        )
      return slsPrint
    })

    jest.mock('../src/util/parser', () => {
      const fs = jest.requireActual('fs')

      const slsPrintWarning = fs.readFileSync(
        require('path').resolve('__tests__', './print-warning.txt'),
        'utf8'
      )

      const parse = jest.requireActual('../src/util/parser')
      const parsed = () =>
        parse(slsPrintWarning, jest.requireActual('../src/util/config')())
      return parsed
    })
    jest.mock('fs', () => {
      return {
        existsSync: () => true,
        readFileSync: () => false
      }
    })
    const result = await validate()
    expect(result).toBe(0)
    done()
  })

  it('Should return 0 (normal exit) without strict mode and strict mode', async done => {
    const validate = require('../src/core/validate')

    jest.mock('../src/util/arguments', () => {
      return { strict: false, stage: 'dev', debug: true }
    })

    jest.mock('../src/util/slsprinter', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = async () =>
        Promise.resolve(
          fs.readFileSync(
            require('path').resolve('__tests__', './print-warning.txt'),
            'utf8'
          )
        )
      return slsPrint
    })

    jest.mock('../src/util/parser', () => {
      const fs = jest.requireActual('fs')

      const slsPrintWarning = fs.readFileSync(
        require('path').resolve('__tests__', './print-warning.txt'),
        'utf8'
      )

      const parse = jest.requireActual('../src/util/parser')
      const parsed = () =>
        parse(slsPrintWarning, jest.requireActual('../src/util/config')())
      return parsed
    })
    jest.mock('fs', () => {
      return {
        existsSync: () => true,
        readFileSync: () => false
      }
    })
    const result = await validate()
    expect(result).toBe(0)
    done()
  })

  it('Should return 0 and warn about repo name convention', async done => {
    process.env.CI = true
    process.env.BITBUCKET_REPO_SLUG = 'dont-follow-any-convention'

    const validate = require('../src/core/validate')

    jest.mock('../src/util/arguments', () => {
      return { strict: false, stage: 'dev' }
    })

    jest.mock('../src/util/slsprinter', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = async () =>
        Promise.resolve(
          fs.readFileSync(
            require('path').resolve('__tests__', './print-warning.txt'),
            'utf8'
          )
        )
      return slsPrint
    })

    jest.mock('../src/util/parser', () => {
      const fs = jest.requireActual('fs')

      const slsPrintWarning = fs.readFileSync(
        require('path').resolve('__tests__', './print-warning.txt'),
        'utf8'
      )

      const parse = jest.requireActual('../src/util/parser')
      const parsed = () =>
        parse(slsPrintWarning, jest.requireActual('../src/util/config')())
      return parsed
    })
    jest.mock('fs', () => {
      return {
        existsSync: () => true,
        readFileSync: () => false
      }
    })
    const result = await validate()
    expect(result).toBe(0)
    done()
  })

  it('Should return 1 (fail exit) due to strict mode', async done => {
    const validate = require('../src/core/validate')

    jest.mock('../src/util/arguments', () => {
      return { strict: true, stage: 'dev' }
    })

    jest.mock('../src/util/slsprinter', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = async () =>
        Promise.resolve(
          fs.readFileSync(
            require('path').resolve('__tests__', './print-warning.txt'),
            'utf8'
          )
        )
      return slsPrint
    })

    jest.mock('../src/util/parser', () => {
      const fs = jest.requireActual('fs')

      const slsPrintWarning = fs.readFileSync(
        require('path').resolve('__tests__', './print-warning.txt'),
        'utf8'
      )

      const parse = jest.requireActual('../src/util/parser')
      const parsed = () =>
        parse(slsPrintWarning, jest.requireActual('../src/util/config')())
      return parsed
    })
    jest.mock('fs', () => {
      return {
        existsSync: () => true,
        readFileSync: () => false
      }
    })
    const result = await validate()
    expect(result).toBe(1)
    done()
  })

  it('Should return 1 (fail exit) due diferent stages provided', async done => {
    const validate = require('../src/core/validate')

    jest.mock('../src/util/arguments', () => {
      return { strict: false, stage: 'prod' }
    })

    process.env.API_STAGE = 'dev'

    jest.mock('../src/util/slsprinter', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = async () =>
        Promise.resolve(
          fs.readFileSync(
            require('path').resolve('__tests__', './print-warning.txt'),
            'utf8'
          )
        )
      return slsPrint
    })

    jest.mock('../src/util/parser', () => {
      const fs = jest.requireActual('fs')

      const slsPrintWarning = fs.readFileSync(
        require('path').resolve('__tests__', './print-warning.txt'),
        'utf8'
      )

      const parse = jest.requireActual('../src/util/parser')
      const parsed = () =>
        parse(slsPrintWarning, jest.requireActual('../src/util/config')())
      return parsed
    })
    jest.mock('fs', () => {
      return {
        existsSync: () => true,
        readFileSync: () => false
      }
    })
    const result = await validate()
    expect(result).toBe(1)
    done()
  })

  it('Should return 1 (fail exit) due to custom validation fail', async done => {
    const validate = require('../src/core/validate')

    jest.mock('../src/util/arguments', () => {
      return { strict: true }
    })

    jest.mock('../src/util/config', () => {
      const custom = JSON.stringify({
        custom: [
          {
            key: 'providers',
            value: true
          }
        ]
      })
      const readConf = () => jest.requireActual('../src/util/config')(custom)
      return readConf
    })

    jest.mock('../src/util/slsprinter', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = async () =>
        Promise.resolve(
          fs.readFileSync(
            require('path').resolve('__tests__', './print-warning.txt'),
            'utf8'
          )
        )
      return slsPrint
    })

    jest.mock('../src/util/parser', () => {
      const fs = jest.requireActual('fs')

      const slsPrintWarning = fs.readFileSync(
        require('path').resolve('__tests__', './print-warning.txt'),
        'utf8'
      )

      const parse = jest.requireActual('../src/util/parser')
      const parsed = () =>
        parse(slsPrintWarning, jest.requireActual('../src/util/config')())
      return parsed
    })
    jest.mock('fs', () => {
      return {
        existsSync: () => true,
        readFileSync: () => false
      }
    })
    const result = await validate()
    expect(result).toBe(1)
    done()
  })

  it('Should return 1 (fail exit) due to required validation fail', async done => {
    const validate = require('../src/core/validate')

    jest.mock('../src/util/arguments', () => {
      return { strict: true }
    })

    jest.mock('../src/util/config', () => {
      const custom = JSON.stringify({
        custom: [
          {
            key: 'providers',
            value: true
          }
        ]
      })
      const readConf = jest.requireActual('../src/util/config')(custom)

      readConf.req.push(
        JSON.stringify({
          key: 'not-exists',
          value: true
        })
      )
      const ret = () => readConf
      return ret
    })

    jest.mock('../src/util/slsprinter', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = async () =>
        Promise.resolve(
          fs.readFileSync(
            require('path').resolve('__tests__', './print-warning.txt'),
            'utf8'
          )
        )
      return slsPrint
    })

    jest.mock('../src/util/parser', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = fs.readFileSync(
        require('path').resolve('__tests__', './print.txt'),
        'utf8'
      )

      const parse = jest.requireActual('../src/util/parser')
      const parsed = () =>
        parse(slsPrint, jest.requireActual('../src/util/config')())
      return parsed
    })
    jest.mock('fs', () => {
      return {
        existsSync: () => true,
        readFileSync: () => false
      }
    })
    const result = await validate()
    expect(result).toBe(1)
    done()
  })

  it('Should return 0 (normal exit) with custom validation', async done => {
    const validate = require('../src/core/validate')

    jest.mock('../src/util/arguments', () => {
      return { strict: true, stage: 'dev' }
    })

    jest.mock('../src/util/config', () => {
      const custom = JSON.stringify({
        custom: [
          {
            key: 'provider',
            value: true
          }
        ]
      })
      const readConf = () => jest.requireActual('../src/util/config')(custom)
      return readConf
    })

    jest.mock('../src/util/slsprinter', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = async () =>
        Promise.resolve(
          fs.readFileSync(
            require('path').resolve('__tests__', './print.txt'),
            'utf8'
          )
        )
      return slsPrint
    })

    jest.mock('../src/util/parser', () => {
      const fs = jest.requireActual('fs')

      const slsPrintWarning = fs.readFileSync(
        require('path').resolve('__tests__', './print.txt'),
        'utf8'
      )

      const parse = jest.requireActual('../src/util/parser')
      const parsed = () =>
        parse(slsPrintWarning, jest.requireActual('../src/util/config')())
      return parsed
    })
    jest.mock('fs', () => {
      return {
        existsSync: () => true,
        readFileSync: () => true
      }
    })
    const result = await validate()
    expect(result).toBe(0)
    done()
  })

  it('Should return 0 (normal exit) with custom validation and debug mode with strict mode', async done => {
    const validate = require('../src/core/validate')

    jest.mock('../src/util/arguments', () => {
      return { strict: true, debug: true, stage: 'dev' }
    })

    jest.mock('../src/util/config', () => {
      const custom = JSON.stringify({
        custom: [
          {
            key: 'provider',
            value: true
          }
        ]
      })
      const readConf = () => jest.requireActual('../src/util/config')(custom)
      return readConf
    })

    jest.mock('../src/util/slsprinter', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = async () =>
        Promise.resolve(
          fs.readFileSync(
            require('path').resolve('__tests__', './print.txt'),
            'utf8'
          )
        )
      return slsPrint
    })

    jest.mock('../src/util/parser', () => {
      const fs = jest.requireActual('fs')

      const slsPrintWarning = fs.readFileSync(
        require('path').resolve('__tests__', './print.txt'),
        'utf8'
      )

      const parse = jest.requireActual('../src/util/parser')
      const parsed = () =>
        parse(slsPrintWarning, jest.requireActual('../src/util/config')())
      return parsed
    })
    jest.mock('fs', () => {
      return {
        existsSync: () => true,
        readFileSync: () => true
      }
    })
    const result = await validate()
    expect(result).toBe(0)
    done()
  })

  it('Should return 0 (normal exit) with custom validation, debug mode, strict mode and secret value', async done => {
    const validate = require('../src/core/validate')

    jest.mock('../src/util/arguments', () => {
      return { strict: true, debug: true, stage: 'dev' }
    })

    jest.mock('../src/util/config', () => {
      const custom = JSON.stringify({
        custom: [
          {
            key: 'provider',
            value: true,
            secret: true
          }
        ]
      })
      const readConf = () => jest.requireActual('../src/util/config')(custom)
      return readConf
    })

    jest.mock('../src/util/slsprinter', () => {
      const fs = jest.requireActual('fs')
      const slsPrint = async () =>
        Promise.resolve(
          fs.readFileSync(
            require('path').resolve('__tests__', './print.txt'),
            'utf8'
          )
        )
      return slsPrint
    })

    jest.mock('../src/util/parser', () => {
      const fs = jest.requireActual('fs')

      const slsPrintWarning = fs.readFileSync(
        require('path').resolve('__tests__', './print.txt'),
        'utf8'
      )

      const parse = jest.requireActual('../src/util/parser')
      const parsed = () =>
        parse(slsPrintWarning, jest.requireActual('../src/util/config')())
      return parsed
    })
    jest.mock('fs', () => {
      return {
        existsSync: () => true,
        readFileSync: () => true
      }
    })
    const result = await validate()
    expect(result).toBe(0)
    done()
  })
})
