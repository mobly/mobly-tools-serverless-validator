const readConfig = require('../src/util/config')

describe('[CONFIG]', () => {
  it('Should return required fields', done => {
    const configuration = readConfig()

    const requiredConfig = [
      {
        key: 'service',
        value: true
      },
      {
        key: 'provider.name',
        value: 'aws'
      },
      {
        key: 'provider.stackTags.departament',
        value: true
      },
      {
        key: 'provider.stackTags.project',
        value: true
      },
      {
        key: 'provider.stackTags.environment',
        value: true
      },
      {
        key: 'provider.stackTags.service',
        value: true
      },
      {
        key: 'plugins',
        value: 'serverless-newrelic-lambda-layers'
      },
      {
        key: 'custom.newRelic.accountId',
        value: true,
        secret: true
      },
      {
        key: 'custom.newRelic.exclude',
        value: 'warmUpPlugin'
      }
    ]

    expect(configuration).toHaveProperty('req')
    expect(configuration).toHaveProperty('custom')
    expect(configuration.req).toHaveLength(9)
    expect(configuration.req).toEqual(requiredConfig)
    expect(configuration.custom).toHaveLength(0)
    done()
  })

  it('Should return custom properties', done => {
    const custom = JSON.stringify({
      custom: [
        {
          key: 'string',
          value: true
        }
      ]
    })

    const configuration = readConfig(custom)

    expect(configuration).toHaveProperty('req')
    expect(configuration).toHaveProperty('custom')
    expect(configuration.custom).toHaveLength(1)
    expect(configuration.custom).toEqual(JSON.parse(custom).custom)
    done()
  })

  it('Should fail due to not being an object', done => {
    const custom = JSON.stringify({
      custom: ['notObj']
    })

    try {
      readConfig(custom)

      expect(true).toBe(false)
    } catch (e) {
      expect(e).toHaveProperty('module', 'config')
      expect(e).toHaveProperty('error', 'custom[0] : not object')
      done()
    }
  })

  it('Should fail due to key not being a string', done => {
    const custom = JSON.stringify({
      custom: [
        {
          key: true,
          value: true
        }
      ]
    })

    try {
      readConfig(custom)

      expect(true).toBe(false)
    } catch (e) {
      expect(e).toHaveProperty('module', 'config')
      expect(e).toHaveProperty('error', 'custom[0].key must be string')
      done()
    }
  })
  it('Should fail due to value not being a string nor boolean', done => {
    const custom = JSON.stringify({
      custom: [
        {
          key: 'string',
          value: [true]
        }
      ]
    })

    try {
      readConfig(custom)

      expect(true).toBe(false)
    } catch (e) {
      expect(e).toHaveProperty('module', 'config')
      expect(e).toHaveProperty(
        'error',
        'custom[0].value must be boolean or string'
      )
      done()
    }
  })
  it('Should fail due to missing key', done => {
    const custom = JSON.stringify({
      custom: [
        {
          value: [true]
        }
      ]
    })

    try {
      readConfig(custom)

      expect(true).toBe(false)
    } catch (e) {
      expect(e).toHaveProperty('module', 'config')
      expect(e).toHaveProperty('error', 'custom[0] missing key or value')
      done()
    }
  })
})
