const index = require('../index')
describe('[INDEX]', () => {
  it('Should not be null', async (done) => {
    jest.mock('../src/core/validate', () => () => Promise.resolve(0))
    expect(index).not.toBe(null)
    done()
  })
})
