const argv = require('../src/util/arguments')

describe('[ARGV]', () => {
  it('Should have been called', (done) => {
    expect(argv).not.toHaveProperty('s')
    done()
  })
})
