const parse = require('../src/util/parser')
const readConfiguration = require('../src/util/config')
const fs = require('fs')
const path = require('path')

const slsPrintWarning = fs.readFileSync(
  path.join(__dirname, '/print-warning.txt'),
  'utf8'
)
const slsPrint = fs.readFileSync(path.join(__dirname, '/print.txt'), 'utf8')

const config = JSON.stringify({
  custom: [
    {
      key: 'provider.name',
      value: 'aws'
    },
    {
      key: 'provider.region',
      value: true
    }
  ]
})

describe('[PARSER]', () => {
  it('Should have JSON error', done => {
    try {
      const parsed = parse('', readConfiguration())
      if (parsed) expect(true).toBe(false) // if dont throw error, fail test
      done()
    } catch (e) {
      expect(e).toHaveProperty('module', 'parser')
      done()
    }
  })

  it('Should have warnings', done => {
    const parsed = parse(slsPrintWarning, readConfiguration(config))
    expect(parsed).toHaveProperty('warnings')
    expect(parsed).toHaveProperty('validations')
    expect(parsed.validations.required).toContainEqual({
      key: 'provider.name',
      reference: 'aws',
      includes: true
    })
    done()
  })

  it('Should have zero warnings', done => {
    const parsed = parse(slsPrint, readConfiguration(config))
    expect(parsed).toHaveProperty('warnings')
    expect(parsed.warning).toBe(undefined)
    done()
  })
})
