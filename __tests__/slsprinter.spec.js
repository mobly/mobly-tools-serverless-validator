
describe('[SLS PRINTER]' , () => {
    beforeEach(() => jest.resetModules())

    it('Should fail due to file inexistent', async (done) => {
        const slsPrint = require('../src/util/slsprinter')

        jest.mock('util', () => {
    return {
        promisify: (param) => {
            const exec = (cmd) => {
                return { stdout: 'test' }
            }
            return exec
        }
    }
})

        try{
            await slsPrint()
            expect(true).toBe(false)
            done()
        } catch (e){

            expect(e).toHaveProperty('module', 'slsPrinter')
            expect(e).toHaveProperty('error', 'serverless.yml not found in root. Exiting')
            done()
        }
    })

    it('Shoud return test', async (done) => {
        const slsPrint = require('../src/util/slsprinter')

        jest.mock('util', () => {
    return {
        promisify: (param) => {
            const exec = (cmd) => {
                return { stdout: 'test' }
            }
            return exec
        }
    }
})

        try{
            const print = await slsPrint(true)
            expect(print).toBe('test')
            done()
        } catch (e){

            expect(true).toBe(false)
            done()
        }
    })

    it('Shoud return stderr', async (done) => {
        const slsPrint = require('../src/util/slsprinter')

        jest.mock('util', () => {
    return {
        promisify: (param) => {
            const exec = (cmd) => {
                return { stderr: 'test' }
            }
            return exec
        }
    }
})

        try{
            await slsPrint(true)
            done()
        } catch (e){

            expect(e).toHaveProperty('module', 'slsPrinter')
            expect(e).toHaveProperty('error', 'test')
            done()
        }
    })
})
