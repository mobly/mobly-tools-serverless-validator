#!/usr/bin/env node

const slsPrint = require('../util/slsprinter')
const parse = require('../util/parser')
const argv = require('../util/arguments')
const readConfig = require('../util/config')
const util = require('util')
const fs = require('fs')

const { Signale } = require('signale')

const logger = new Signale({
  interactive: false,
  scope: 'serverless-validator'
})

async function validate () {
  const strict = argv.strict

  const stage = process.env.API_STAGE || argv.stage

  if (argv.stage && argv.stage !== stage) {
    logger.fatal(`Two different stages supplied:
                  - stage provided : ${argv.stage} 
                  - env.API_STAGE : ${process.env.API_STAGE}`)
    return 1
  }
  if (!stage) {
    logger.fatal(
      'No stage provided.\nProvide environment API_STAGE or "-e=stage" in CLI options'
    )
    return 1
  }

  const debugMode = argv.debug

  let configJson = ''

  if (strict) {
    logger.info(
      'Running in strict mode. Serverless warning will fail the validation'
    )
  } else {
    logger.warn(
      'Running in non-strict mode. Serverless warning will be printed ONLY'
    )
  }
  if (fs.existsSync('slsvalidate.json')) {
    configJson = fs.readFileSync('./slsvalidate.json', 'utf8')
  }

  try {
    const slsFileExists = fs.existsSync('./serverless.yml')

    const config = readConfig(configJson)
    const failed = []

    if (config.custom.length > 0) {
      logger.info("Found 'slsvalidate.json'. Added custom validations")
    } else {
      logger.info("'slsvalidate.json' not found. Using only defaults")
    }

    logger.await('Retrieving serverless information')
    const slsprint = await slsPrint(slsFileExists, debugMode, stage)
    logger.success('Serverless information loaded')

    const parsed = parse(slsprint, config)

    const { required, custom } = parsed.validations
    const { warnings, information } = parsed

    logger.info('Validating Required serverless config')
    required.forEach(obj => {
      if (obj.value || obj.includes) {
        logger.success(obj)
      } else {
        failed.push(obj)
        logger.error(obj)
      }
    })
    if (custom.length > 0) {
      logger.info('Validating Custom serverless config')
      custom.forEach(obj => {
        if (obj.value || obj.includes) {
          logger.success(obj)
        } else {
          failed.push(obj)
          logger.error(obj)
        }
      })
    }
    if (warnings) {
      logger.warn(`
            ${warnings}
            `)

      if (debugMode) {
        logger.info(`
            ${information.join('\n\n** ')}
          `)
      }

      if (strict) {
        logger.fatal('Terminating process due to strict mode')
        return 1
      } else {
        logger.warn('Continuing the validation step due to non-strict mode')
      }
    }

    if (failed.length > 0) {
      logger.fatal('Validation failed. Check above what failed. Exiting')
      return 1
    } else {
      if (process.env.CI === true) {
        if (!process.env.BITBUCKET_REPO_SLUG.includes('mobly-service')) {
          logger.warn(`Identified CI/CD environment.
                    Consider fixing the name of the repository including:
                    '@mobly/service' in the front of the repo name`)
        }
      }
      logger.success('All fields validated. Normal exit')
      return 0
    }
  } catch (e) {
    switch (e.module) {
      case 'slsPrinter':
        logger.fatal(`
                    Serverless error:
                    ${e.error}
                    `)
        break
      case 'parser':
        logger.fatal(`
                    Parsing error:
                    ${e.error}
                    `)
        break
      case 'config':
        logger.fatal(`
                Configuration error in 'slsvalidation.json':
                ${e.error}
                `)
        break
      default:
        logger.fatal(`
                ERROR >>
                `)
        console.log(util.inspect(e))
        break
    }
  }
  return 1
}

module.exports = validate
