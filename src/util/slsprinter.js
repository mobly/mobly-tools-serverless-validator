const util = require('util')

const exec = util.promisify(require('child_process').exec)

async function slsPrint (fileExists = false, debugMode, stage) {
  try {
    if (fileExists) {
      const { stderr, stdout } = await exec(`${debugMode ? 'SLS_DEBUG=* ' : ''}sls print --format json --stage ${stage || 'dev'}`)
      if (stderr) {
        throw stderr
      }
      return stdout
    } else {
      const message = 'serverless.yml not found in root. Exiting'
      throw message
    }
  } catch (e) {
    const err = {
      module: 'slsPrinter',
      error: e
    }
    throw err
  }
}

module.exports = slsPrint
