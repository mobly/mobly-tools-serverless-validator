const fields = {
  req: [
    {
      key: 'service',
      value: true
    },
    {
      key: 'provider.name',
      value: 'aws'
    },
    {
      key: 'provider.stackTags.departament',
      value: true
    },
    {
      key: 'provider.stackTags.project',
      value: true
    },
    {
      key: 'provider.stackTags.environment',
      value: true
    },
    {
      key: 'provider.stackTags.service',
      value: true
    },
    {
      key: 'plugins',
      value: 'serverless-newrelic-lambda-layers'
    },
    {
      key: 'custom.newRelic.accountId',
      value: true,
      secret: true
    },
    {
      key: 'custom.newRelic.exclude',
      value: 'warmUpPlugin'
    }
  ],
  custom: []
}

function readConfig (configFile) {
  if (configFile) {
    try {
      const config = JSON.parse(configFile)
      fields.custom = config.custom

      const invalidFields = fields.custom
        .map((field, index) => {
          if (typeof field !== 'object') {
            return `custom[${index}] : not object`
          } else {
            if (
              Object.prototype.hasOwnProperty.call(field, 'key') &&
              Object.prototype.hasOwnProperty.call(field, 'value')
            ) {
              const aux = []
              if (typeof field.key !== 'string') {
                aux.push(`custom[${index}].key must be string`)
              }
              // confere se field.value é boolean ou string
              if (!['boolean', 'string'].includes(typeof field.value)) {
                aux.push(`custom[${index}].value must be boolean or string`)
              }
              if (aux.length > 0) {
                // vê se tem erros no auxiliar. retorna a concatenção dos erros
                return aux.join(' and ')
              }
              return false
            } else {
              return `custom[${index}] missing key or value`
            }
          }
        })
        .filter(o => o !== false)
      if (invalidFields.length > 0) {
        throw invalidFields.join('\n')
      }
    } catch (e) {
      const err = {
        module: 'config',
        error: e
      }
      throw err
    }
  }
  return fields
}

module.exports = readConfig
