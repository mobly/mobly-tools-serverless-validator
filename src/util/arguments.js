const argv = require('yargs')
  .command('mobly-serverless-validator', 'Validates the serverless configuration')
  .options({
    'debug': {
        alias: 'd',
        describe: 'Sets the SLS_DEBUG=* for better error visualization',
        boolean: true,
        default: false
    },
    'strict': {
      alias: 's',
      describe: 'Setting this to true, pipeline will fail with an error on Serverless warnings',
      boolean: true,
      deafult: false
    },
    'stage': {
      alias: 'e',
      describe: 'The stage in which serverless will validate the configuration',
      default: 'dev'
    }
  }).argv

module.exports = argv
