const _ = require('lodash')

function parse (slsprint, keys) {
  const result = {
    validations: {
      required: [],
      custom: []
    },
    warnings: '',
    information: ''
  }

  const catchBlocks = (regex, str) => {
    let result,
      indices = [],
      strings = { warning: [], information: [] }
    while ((result = regex.exec(str))) {
      indices.push(result.index)
    }
    for (let index = 0; index < indices.length; index++) {
      const sub = str.substring(indices[index], indices[index + 1])
      if (sub.includes('Serverless Information')) {
        strings.information.push(
          sub.replace(/Serverless Information (-*)/gi, '').trim()
        )
      } else {
        strings.warning.push(
          sub.replace(/Serverless Warning (-*)/gi, '').trim()
        )
      }
    }
    return strings
  }

  try {
    // extrai o JSON como texto
    const objStr = slsprint.substring(
      slsprint.indexOf('{'),
      slsprint.lastIndexOf('}') + 1
    )
    const obj = JSON.parse(objStr)
    const infoStr = slsprint.replace(objStr, '')
    const { information, warning } = catchBlocks(
      /Serverless (Warning|Information) (-*)/gi,
      infoStr
    )
    result.information = information
    if (warning.length > 0) {
      result.warnings = `
              *** NOTE THAT YOU HAVE WARNINGS ABOUT YOUR SERVERLESS CONFIGURATIONS ***
        ====================================================================================
        
        ${warning.join(`
        
        ** `)}

        ====================================================================================

        ** BE SURE THAT THIS WARNINGS WILL NOT AFFECT THE FUNCTIONALITIES OF YOUR SERVICE **

        `
    }

    const { req, custom } = keys
    for (const index in req) {
      if (typeof req[index].value === 'boolean') {
        result.validations.required.push({
          key: req[index].key,
          value: req[index].secret
            ? '[hidden]'
            : _.get(obj, req[index].key, undefined)
        })
      } else {
        let aux = _.get(obj, req[index].key, undefined)
        if (Array.isArray(aux)) {
          aux = aux.includes(req[index].value)
        } else if (typeof aux === 'string') {
          aux = aux === req[index].value
        }
        result.validations.required.push({
          key: req[index].key,
          reference: req[index].value,
          includes: aux
        })
      }
    }
    for (const index in custom) {
      if (typeof custom[index].value === 'boolean') {
        result.validations.custom.push({
          key: custom[index].key,
          value: custom[index].secret
            ? '[hidden]'
            : _.get(obj, req[index].key, undefined)
        })
      } else {
        let aux = _.get(obj, custom[index].key, undefined)
        if (Array.isArray(aux)) {
          aux = aux.includes(custom[index].value)
        } else if (typeof aux === 'string') {
          aux = aux === custom[index].value
        }
        result.validations.required.push({
          key: custom[index].key,
          reference: custom[index].value,
          includes: aux
        })
      }
    }

    return result
  } catch (e) {
    const err = {
      module: 'parser',
      error: e
    }
    throw err
  }
}

module.exports = parse
