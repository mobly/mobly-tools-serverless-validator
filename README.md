# @mobly/tools/serverless-validator

> This is a repository containing a script that will be used inside projects CI/CD pipelines to evaluate if the project is following mobly minimum security and quality compliances.

Este pacote serve para validar informações básicas que necessariamente devem estar em seu arquivo serverless. Estas informações são:

```
- Nome do Serviço
- Tags (estas devem incluir project, team, environment e departament)
```

É possível também adicionar novas chaves através de um arquivo `slsvalidate.json`, explicado mais abaixo em **Uso Avançado**.

## Requerimentos

- Serverless;
- Node Js;
- Ter as variáveis de ambiente AWS_ACCESS_KEY_ID e AWS_SECRET_ACCESS_KEY configuradas ou o ambiente do serverless com acesso ao stack criado;
- Arquivo `serverless.yml` na raíz do projeto;
- Comando deve ser executado na raiz do projeto;

### Dependência Obrigatória

Execute `npm --save-dev serverless-newrelic-lambda-layers`. Feito isso, adicionar no `serverless.yml` na raíz do projeto:

```
plugins:
  - serverless-webpack
  - serverless-newrelic-lambda-layers # Deve vir sempre DEPOIS do plugin do webpack
```

> Atente-se que é **obrigatório manter a order desses 2 plugins**. Caso contrário, resultará em falha!

Dentro de custom do mesmo arquivo, insira:

```
newRelic:
    accountId: ${ssm:/${self:custom.stage}/new-relic-id}
    exclude:
      - warmUpPlugin
```

A chave `accountId` deverá estar setada no SSM na AWS.

## Uso Básico

Você pode usar o pacote de duas formas:

    - Instalando globalmente

```
npm i -g @moblybr/tools-serverless-validator
```

    - Através do npx

```
npx @moblybr/tools-serverless-validator
```

### Argumentos da CLI

#### -d, --debug [Opcional, default: false]

    Exibe as informações de debug e quaisquer serverless information que seria retornado no comando `SLS_DEBUG=* sls print --format json`

#### -e, --stage [Opcional, default: 'dev']

    Para verificar algum environment/stage diferente de Dev, existe a opção `-e <stage>`. Caso você tenha setado uma variável de ambiente (`API_STAGE`) e ela for diferente da que foi passada, será acusado um erro e o processo irá falhar com a seguinte mensagem:

        ```
        Two different stages supplied:
        - stage provided : dev
        - env.API_STAGE : staging
        ```

#### -s, --strict [Opcional, default: false]

    Caso você tenha algum warning que venha do serverless (por baixo dos panos roda o comando `sls print --format json`), ele será apenas exibido mas não irá interromper o processo, caso esteja em ambiente de CI/CD (considerando o bitbucket como referência).

    Para evitar isso e fazer o pipeline falhar, execute o comando com o argumento `-s` ou `--strict`.

    Caso tenha algum _warning_ na sua configuração serverless, o processo irá falhar e o _warning_ será exibido no console.

## Retornos Esperados

Caso os campos não tenham sido validados corretamente, ele retornará um erro indicando a chave que falhou e o valor de retorno no formato key/value.

Ex:

```
[serverless-validator] › ✔  success   { key: 'provider.stackTags.service', value: 'boilerplate-api-node' }
```

Ou

```
[serverless-validator] › X  error   { key: 'provider.stackTags.service', value: undefined }
```

> Este formato retornará caso seja alguma chave que esteja presente e não tenha um valor especificado.

Caso alguma chave tenha um valor específico, o retorno será:

```
[serverless-validator] › ✔  success   {
  key: 'plugins',
  reference: 'serverless-plugin-epsagon',
  includes: true
}
```

Em que é verificado de a chave em questão tem o valor idêntico ao informado, de tal forma que o campo `includes` indica se a chave é igual ao valor informado ou se este valor está presente em um array de valores.

## Uso Avançado

É possível configurar campos adicionais para a verificação, que devem ser explicitados em um arquivo `slsvalidate.json`, como o exemplo abaixo:

```json
{
  "custom": [
    {
      "key": "no.pai.filho", // ${String} - Obrigatório
      "value": true, // ${String | Boolean<true>} - Obrigatório
      "secret": true // ${Boolean<true>} - Opcional
    }
  ]
}
```

**Atente-se:**

- Deve contar com o objeto `custom`, que é uma **_ARRAY_**,
- O campo `key` precisa do caminho total da chave procurada,
- O campo `value` aceita somente `true` ou `string`, para conferência da presença de uma chave ou para a conferência do valor exato daquela chave, respectivamente,
- O campo `secret` transforma o valor a ser exibido em `[hidden]`, para ocultar valores que são sensíveis e não devem ser exibidos nos logs de pipelines;
- Um arquivo JSON não válido interromperá o processo e indicará qual o erro na sua configuração,
- O arquivo `slsvalidate.json` deve estar na raiz do projeto,
- Os campos padrão não podem ser alterados, mas caso seja necessário fazer alguma específica, só adicionar o mesmo item no seu arquivo custom,
- Mesmo as falhas em custom fazem que o processo resulte em falha.
