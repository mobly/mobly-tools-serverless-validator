# [1.1.0](https://bitbucket.org/mobly/mobly-tools-serverless-validator/compare/v1.0.2...v1.1.0) (2020-03-23)


### Features

* Colocando new relic como obrigatorio. Refactor de modo debug e modo sensitive ([1c5f170](https://bitbucket.org/mobly/mobly-tools-serverless-validator/commits/1c5f170))

## [1.0.2](https://bitbucket.org/mobly/mobly-tools-serverless-validator/compare/v1.0.1...v1.0.2) (2020-03-02)


### Bug Fixes

* Removendo epsagon da lista de obrigatoriedades ([e05e147](https://bitbucket.org/mobly/mobly-tools-serverless-validator/commits/e05e147))

## [1.0.1](https://bitbucket.org/mobly/mobly-tools-serverless-validator/compare/v1.0.0...v1.0.1) (2020-02-27)


### Bug Fixes

* Colocando stage default. Alterando documentação ([b081a82](https://bitbucket.org/mobly/mobly-tools-serverless-validator/commits/b081a82))

# 1.0.0 (2019-10-29)


### chore

* **pipelines:** First release ([5c7e3db](https://bitbucket.org/mobly/mobly-tools-serverless-validator/commits/5c7e3db))


### BREAKING CHANGES

* **pipelines:** First release
